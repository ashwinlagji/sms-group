import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

import { from, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private httpClient: HttpClient){

    }

    getData( params ){
        const apiUrl = 'api/records';
        return this.httpClient.request('GET', apiUrl, {responseType:'json', params})
        .pipe(
            map((ResponseData: { status: String, type: String, count: Number, data }) => {
              return ResponseData.data;
            }), catchError( error => {
              return throwError( 'Something went wrong!' );
            })
         );
    }
}
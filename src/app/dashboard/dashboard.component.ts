import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  MatSort
} from '@angular/material/sort';
import {
  MatTableDataSource
} from '@angular/material/table';

import {
  ApiService
} from './../api.service';
import {
  MatPaginator
} from '@angular/material/paginator';
import {
  FormGroup,
  FormControl
} from '@angular/forms';

export interface Record {
  "id": String,
  "city": String,
  "start_date": Date,
  "end_date": Date,
  "price": Number,
  "status": String,
  "color": String
}

/**
 * @title Table with sorting
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  RECORDS_DATA: Record[];
  displayedColumns: string[] = ['ID', 'City', 'Start Date', 'End Date', 'Price', 'Status', 'Color'];
  dataSource = new MatTableDataSource(this.RECORDS_DATA);

  startDate = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  endDate = new FormGroup({
    start: new FormControl(),
    end: new FormControl()
  });

  @ViewChild(MatSort, {
    static: true
  }) sort: MatSort;

  @ViewChild(MatPaginator, {
    static: true
  }) paginator: MatPaginator;

  constructor(private apiService: ApiService) {

  }

  ngOnInit() {
    this.refreshData({});
  }

  refreshData(filter: any) {
    this.apiService.getData(filter).subscribe(
      responseData => {
        if (responseData) {
          this.dataSource = new MatTableDataSource( < Record[] > responseData);
          this.dataSource.sortingDataAccessor = (item, property) => {
            let properties = {
              'ID': 'id',
              'City': 'city',
              'Start Date': 'start_date',
              'End Date': 'end_date',
              'Price': 'price',
              'Status': 'status',
              'Color': 'color'
            };
            return item[properties[property]];
          };
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  filterTable() {
    if ((!this.startDate.value.start && !this.startDate.value.end) || (!this.endDate.value.start && !this.endDate.value.end)) {

      // Message can be set for end Uer to select date range 
      return;
    } else {
      const dateRange = {
        startdate: {
          start_date: this.startDate.value.start.toISOString(),
          end_date: this.startDate.value.end.toISOString()
        },
        enddate: {
          start_date: this.endDate.value.start.toISOString(),
          end_date: this.endDate.value.end.toISOString()
        }
      }
      this.refreshData({
        dateRange: JSON.stringify(dateRange)
      });
    }
  }

  clearDate() {
    this.startDate.reset();
    this.endDate.reset();
    this.refreshData({});
  }
}

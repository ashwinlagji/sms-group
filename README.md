# SMS Group

The project covers the basic NodeJS server with crud operations and Angular webapp with material design.

## Installation

### Requirements

- NodeJS v10.0.0
- npm
- Angular v9
- Angular Material
- MongoDB v4.2

### DB Setup

Download and install the MongoDB community server on the local machine.
Follow the below command to start the database instance.

```shell
$ 
$ mongod --dbpath=<path/to/store/DB/files>
$ 
```

### Clone

Clone this repo to your local machine using `git clone git@bitbucket.org:ashwinlagji/sms-group.git`

```shell
$ 
$ git clone git@bitbucket.org:ashwinlagji/sms-group.git
$ 
```

### Setup

In order to able to run the code please install the dependencies. The dependencies are specified in the package.json file

now install npm and bower packages

```shell
$ 
$ npm install
$ 
```

Install nodemon which is used to manage the NodeJS server instance
```shell
$ 
$ npm install -g nodemon
$ 
```

## Running the project

Please follow below steps after sucessful package installation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.

## Development server

Run `npm start` to start the angular dev server and NodeJS server instance. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Note: 

Incase facing issues running both the servers using npm start( sometimes an issue on windows systems) please start both the servers manually using below commands.
```shell
$ 
$ nodemon ./server/bin/www
$ ng serveng serve --proxy-config proxy.conf.json
```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
The build files can be hoisted on any of the web server


## Running unit tests

The Project doest cover any test cases and this can be ignored.
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

The Project doest cover end to end cases as well.
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help
 Please drop a main at ashwinlagji@gmail.com in case of any difficulty getting the project working.
const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let activitySchema = new mongoose.Schema({
  "id": String,
  "city": String,
  "start_date": { "type": Date, "default": Date.now },
  "end_date": { "type": Date, "default": Date.now },
  "price": Number,
  "status": {
    type: String,
    // enum    :  ['Often', 'Weekly', 'Daily', 'Monthly', 'Once', 'Never', 'Seldom', 'Yearly']
  },
  "color": String
});

module.exports = mongoose.model('record', activitySchema);
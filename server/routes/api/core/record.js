const Record = require('../misc/record.model');
const _ = require('lodash');

recordSeedData = require( './mockData/data.json' );

module.exports = {

  reset: (req, res) => {

    try {
          // clear all existing documents from the collections
          Record.deleteMany({}, () => {
            console.log("Database Reset !!")
          });

          // populate the record collection from json data
          for( var i = 0; i < recordSeedData.length; i++ ) {
            new Record( recordSeedData[ i ] ).save();
          }

          return res.status(200).json({status: true, type: "reset", message: "Database has been rest"});
    } catch (error) {
      console.log(error);
      return res.status(500).json({status: false, error})
    }
  },

  getAll: async (req,res) => {
    try {

      let filter = {};

      try {

      let dateRange = JSON.parse(req.query.dateRange)

      let startdate = dateRange.startdate || undefined;
      let enddate = dateRange.enddate || undefined;
      
      let startDate = {
        start_date: new Date(startdate.start_date),
        end_date: new Date(startdate.end_date)
      }
      let endDate = {
        start_date: new Date(enddate.start_date),
        end_date: new Date(enddate.end_date)
      }

      if(startdate && enddate){
        filter = {
            'start_date': { '$lte' : startDate.start_date, '$gte':  startDate.end_date },
            'end_date': { '$lte' : endDate.start_date, '$gte':  endDate.end_date }
      }
    }

    filter = {  start_date: { '$gte': new Date(startdate.start_date), '$lte': new Date(startdate.end_date)  }, end_date: { '$gte': new Date(enddate.start_date), '$lte': new Date(enddate.end_date) }}
    }catch(error){
      console.log(error);
      filter = {};
    }

      let records_total = await Record.find(filter);
      let count = records_total.length;
      return res.status(200).json({status: true, type: "Records",  count, data: records_total});
    } catch (error) {
      return res.status(500).json({status: false, error})
    }
  },

  get: async (req,res) => {
    const page = req.query.page || 1;
    const limit = req.query.limit || 20;
    try {
      let records_total = await Record.find();
      let count = records_total.length;
      let records = await Record.find()
                                .skip((page-1)*limit)
                                .limit(limit);
      return res.status(200).json({status: true, count, records});
    } catch (error) {
      console.log(error);
      return res.status(500).json({status: false, error})
    }
  },

  findById: async (req,res) => {
    const id = req.params.id;
    const filter = req.query.filter || {};
    try {
      let records_total = await Record.find();
      let count = records_total.length;
      let records = await Record.find({id: id});
      return res.status(200).json({status: true, count, records});
    } catch (error) {
      console.log(error);
      return res.status(500).json({status: false, error})
    }
  },

  create: async (req,res) => {
    try {
      let record = await Record.create(req.body);
      return res.status(200).json({status: true, record});
    } catch (error) {
      console.log(error);
      res.status(500).json({status: false, error});
    }
  },

  deleteById: async (req,res) => {
    let record_id = req.params.id;
    if(!record_id) {
      return res
          .status(400)
          .json({ status: false, error: 'Invalid Record id'})
    }
    try {
      await Record.findByIdAndRemove(record_id);
      return res.status(200).json({status:true, message: 'Record Deleted'});
    } catch (error) {
      console.log(error);
      return res.status(500).json({ status: false, error: error })
    }
  },

  editByID : async (req,res) => {
    let record_id = req.params.id;
    if(!record_id) {
      return res
          .status(400)
          .json({ status: false, error: 'Invalid Record'})
    }
    let _record = {
      "id": req.body.id ? req.body.id : undefined,
      "city": req.body.city ? req.body.city : undefined,
      "start_date": req.body.start_date ? new Date (req.body.start_date) : Date.now,
      "end_date": req.body.end_date ? new Date (req.body.end_date) : Date.now,
      "price": req.body.price ? req.body.price : undefined,
      "status": req.body.status ? req.body.status : undefined,
      "color": req.body.color ? req.body.color : undefined
      }
    try {
      let record = await Record.findByIdAndUpdate(record_id, _record, {new: true});
      return res.status(200).json({status: true, record});
    } catch (error) {
      console.log(error)
      return res.status(500).json({ status: false, error: error })
    }
  },

  import: async (req,res) => {
    try {

      /**
       * 
       * Implement method to upload json and import into the database
       * 
       */
      
      return res.status(200).json({status: true, message:  'WIP: Under Maintainance'})

    } catch (error) {
      console.log(error);
      return res.status(500).json({status:false, error: error})
    }
  }
}
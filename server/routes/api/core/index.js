const router = require('express').Router();
const recordsController = require('./record');

router.get('/records', recordsController.getAll);
router.get('/records/:id', recordsController.getAll);
router.put('/records/reset', recordsController.reset);


module.exports = router;
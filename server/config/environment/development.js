'use strict';
// Development specific configuration
// ==================================
const localDB = {
  host: 'localhost',
  port: '27017',
  user: 'admin',
  password: 'admin',
  db: 'sms-group',
}

const remoteDB = {
  host: 'localhost',
  port: '27017',
  user: '',
  password: '',
  database: 'sms-group',

}

module.exports = {
  hostname: 'https://127.0.0.1:8181/',
  connection: localDB,
  secretKey: 'thisissecret'
}
'use strict';
// Development specific configuration
// ==================================
const localDB = {
  host: 'localhost',
  port: '27017',
  user: '',
  password: '',
  db: 'sms-group',

}

const remoteDB = {
  host: 'localhost',
  port: '27017',
  user: '',
  password: '',
  db: 'sms-group'
}

module.exports = {
  hostname: 'https://127.0.0.1:8181/',
  connection: remoteDB,
  secretKey: 'thisissecret'
}
